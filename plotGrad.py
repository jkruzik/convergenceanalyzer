import os
import glob
import re

import matplotlib
import matplotlib.pyplot as plt

DATADIR = '.'
cntm = 0 #number of runs (used for computing avg)
HEADER = "Gradient;NrmgEQ;outertol;innertol;outeriter"

def getValue(arr,stri):
    try:
        idx = arr.index(stri)
        idx += 1
    except ValueError:
        idx = 0
    if idx:
        return arr[idx]
    else:
        return 'NA'

def append(arr,idx,val):
    arr[idx].append(val)
    #print('%s;' % arr[idx][-1],end='')
    return idx+1

def printDat(dat):
    print(HEADER)
    for i in range(len(dat[0])):
        for j in range(len(dat)):
            print("%s;" % dat[j][i],end='')
        print('')

def plotDat(dat,filename):
    fig, ax = plt.subplots()
    ax.semilogy(dat[0],'r')
    ax.semilogy(dat[1],'g')
    ax.semilogy(dat[3],'k',linewidth=.75)
    ax.semilogy(dat[2],'b',linewidth=.75)
    
    ax2 = ax.twiny()
    ax2.set_xticks(dat[4])
    ax2.set_xticklabels([])

    ax.set_xlim(left=0)
    ax.set(xlabel='Iteration')
    ax.legend(('Gradient', 'NrmEQ', 'Inner stopping crit', 'Outer stopping crit'))
    ax.grid()
    
    fig.savefig("%s.pdf" % filename)
    plt.show()

def main(filename):
    monitor = False;
    dat = [[] for i in range(HEADER.count(';')+1)]
    it = 0; 
    f = open(filename,'r')
    idx = 0
    for line in f:
        s = line.split()
        if s:
            if s[0] == "BEGIN" and s[1] == "outer":
                monitor = True
                shift = 0
                dat[4].append(it)
            #print(s)
            if monitor and s[0].isdigit() and s[1] != "END":
                it += 1
                print(s);
                dat[0].append(float(s[1+shift]))
                dat[1].append(float(s[2+shift]))
                dat[2].append(float(s[7+shift]))
                dat[3].append(float(s[10+shift]))
                if not shift:
                    shift = 1
            if s[0] == "END" and s[1] == "outer":
                monitor = False
    plotDat(dat,filename)

if __name__ == "__main__":
    import sys
    main(sys.argv[1])

