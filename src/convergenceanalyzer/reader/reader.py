import os
import glob
import re

import matplotlib
import matplotlib.pyplot as plt

DATADIR = '.'
cntm = 0 #number of runs (used for computing avg)
fields = ["Gradient","Norm EQ","outer tol","inner tol","outeriter"]

def reader_txt_smalxe(filename):
    data = {"outer": [], "inner": []}
    with open(filename,'r') as f:
        monitor = False # in monitor section
        c_begin = False # saw begin outer
        for line in f:
            s = line.split()
            if s:
                if s[0] == "BEGIN" and s[1] == "outer":
                    monitor = True
                    shift = 0 # 0 = 1st iter
                    c_begin = True;
                    data["outer"].append({})
                    continue
                if c_begin:
                    data["outer"][-1]["M1"] = float(s[0])
                    data["outer"][-1]["rho"] = float(s[1])
                    data["outer"][-1]["eta"] = float(s[2])
                    data["outer"][-1]["gtol"] = float(s[3])
                    data["outer"][-1]["rtol_E"] = float(s[4])
                    if not data["inner"]:
                        data["outer"][-1]["iter_start"] = 0;
                    else:
                        data["outer"][-1]["iter_start"] = data["outer"][-1]["iter_end"] +1;
                    c_begin = False
                    continue
                if s[0] == "END" and s[1] == "outer":
                    monitor = False
                    data["outer"][-1]["iter_end"] = len(data["inner"][-1]) -1;
                    continue
                #print(s)
                if monitor and s[0].isdigit() and s[1] != "END":
                    row = {
                            "g": float(s[1+shift]),
                            "E": float(s[2+shift]),
                            "ttol_outer": float(s[7+shift]),
                            "tol_eq": float(s[10+shift]),
                            "M1Bx": float(s[-1])
                            }
                    if shift:
                        row["step"] = s[1]
                    else:
                        data["outer"][-1]["ttol_outer"] = float(s[7])

                    data["inner"].append(row)

                    if not shift:
                        shift = 1
        return(data)
        #plotDat(dat,filename)

def reader_txt_mpgp_analytics(s):
    keys = ("Hessian","CG","expansion","proportioning","cost","fallbacks")
    if s[0] == "number":
        for k in keys:
            if s[2] == k:
                return (k,s[-1])
    return None


def reader_txt_mpgp(filename):
    data = {"outer": [], "inner": [], "analytics":[{}]}
    with open(filename,'r') as f:
        shift = 0
        for line in f:
            s = line.split()
            if s:
                if s[0].isdigit() and s[1] == "MPGP":
                    if not int(s[0]):
                        shift = 1
                    print(s)
                    print(s[3+shift][7:-1],s[4+shift][7:-1],s[5+shift][7:-1],s[6+shift][6:])
                    row = {
                            "gp": float(s[3+shift][7:-1]),
                            "gf": float(s[4+shift][7:-1]),
                            "gc": float(s[5+shift][7:-1]),
                            "alpha": float(s[6+shift][6:]),
                            }
                    if not shift:
                        data["inner"][-1]["step"] = s[2][1:-1] # the information about the step type is shifted +1
                    else:
                        shift = 0

                    data["inner"].append(row)
                if s[0].isdigit() and s[2] == "Cost":
                        data["inner"][-1]["cost"] = float(s[-1])
                vals = reader_txt_mpgp_analytics(s)
                if vals:
                    data["analytics"][-1][vals[0]] = vals[1]
        data["inner"][-1]["step"] = "-"

        return(data)

if __name__ == "__main__":
    import sys
    reader_txt_smalxe(sys.argv[1])

