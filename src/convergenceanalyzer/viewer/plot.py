import matplotlib
import matplotlib.pyplot as plt
from math import ceil,log10,copysign

plt.rc('ps', usedistiller='xpdf')
plt.rc('mathtext', fontset='cm')

def get_column(data,name):
    return [d[name] for d in data]

def plot_smalxe(data,filename):
    fig, ax = plt.subplots()
    ax.semilogy(get_column(data["inner"],"g"),'r')
    ax.semilogy(get_column(data["inner"],"E"),'g')
    ax.semilogy(get_column(data["inner"],"tol_eq"),'b',linewidth=.75)
    ax.semilogy(get_column(data["inner"],"ttol_outer"),'k',linewidth=.75)
    
    #ax2 = ax.twiny()
    #ax2.set_xticks(dat[4])
    #ax2.set_xticklabels([])

    ax.set_xlim(left=0)
    ax.set(xlabel='Iteration')
    ax.legend(('Gradient', 'NrmEQ', 'Inner stopping crit', 'Outer stopping crit'))
    ax.grid()
    
    fig.savefig("%s.pdf" % filename)
    plt.show()

def plot_mpgp(data,filename):
    maxlength = 100
    gp = get_column(data["inner"],"gp")
    gf = get_column(data["inner"],"gf")
    gc = get_column(data["inner"],"gc")
    step = get_column(data["inner"],"step")
    cost = get_column(data["inner"],"cost")
    length = len(step)
    nplots = ceil(len(gp)/maxlength)
    
    begin = 0
    end = maxlength
    for idxplot in range(nplots):
        if end > length:
            end = length
        fig, ax = plt.subplots()
        print(list(range(begin,end)))
        ax.semilogy(list(range(begin,end)),gp[begin:end],'r',label='Projected gradient')
        ax.semilogy(list(range(begin,end)),gf[begin:end],'b',linewidth=.75,label='Free gradient')
        ax.semilogy(list(range(begin,end)),gc[begin:end],'k',linewidth=.75,label='Chopped gradient')

        step_exp_x = []
        step_exp_y = []
        for i in range(begin,end):
            if step[i] == "e":
                step_exp_x.append(i)
                step_exp_y.append(gf[i])

        ax.plot(step_exp_x,step_exp_y,marker='x',markersize=6,linestyle='None',color='red',label='Expansion')

        step_prop_x = []
        step_prop_y = []
        for i in range(begin,end):
            if step[i] == "p":
                step_prop_x.append(i)
                step_prop_y.append(gc[i])

        ax.plot(step_prop_x,step_prop_y,marker='o',markersize=6,linestyle='None',color='black',label='Proportioning')

        step_fall_x = []
        step_fall_y = []
        for i in range(begin,end):
            if step[i] == "f":
                step_fall_x.append(i)
                step_fall_y.append(gf[i])

        ax.plot(step_fall_x,step_fall_y,marker='*',markersize=6,linestyle='None',color='green',label='Fallback')
        
        ax.set_xlim(left=begin,right=end)
        ax.set(xlabel='Iteration')
        ax.set(ylabel='Gradient value')
        ax.grid()

        ax2 = ax.twinx()
        #ax2.set_yscale('symlog')
        ax2.plot(range(begin,end),cost[begin:end],'g',label='Cost function')
        ax2.set(ylabel='Cost function value')
        ymin = min(cost[begin:end])
        ymax = max(cost[begin:end])
        p = 10**(copysign(1,ymin)*2)
        if ymax > p*ymin:
            ymax = p*ymin
            #p = log10(abs(ymin))
            #print(ymin,ymax,int(ceil(ymax/10**p)*10**p))
            #ax2.set_ylim(bottom=None,top=int(ceil(ymax/10**p)*10**p))
            ax2.set_ylim(ymin,ymax)


        handles,labels = [(a+b) for a,b in zip(ax.get_legend_handles_labels(),ax2.get_legend_handles_labels())]
        lgd = plt.legend(handles,labels,loc='lower left',ncol=3,mode='expand',bbox_to_anchor=(0., 1.0, 1., .0),borderpad=.0,borderaxespad=0.25,frameon=False)

        fig.savefig("%s_%d.eps" % (filename,idxplot),bbox_inches='tight',bbox_extra_artists=[lgd,])
        
        fname = filename.split('/')[-1]
        run = fname.split('_')
        with open(filename+'.tex','a') as f:
            fstring  = "\\begin{figure}[h]\n\centering\n"
            fstring += "\includegraphics[width=\\textwidth]{%s_%d}\n" % (fname,idxplot)
            fstring += "\caption{%s %s %s}\n" % (run[0],run[1],run[2])
            fstring += "\label{fig:%s_%d}\n" % (fname,idxplot)
            fstring += "\end{figure}\n"
            f.write(fstring)
        begin += maxlength
        end += maxlength
    #plt.show()

