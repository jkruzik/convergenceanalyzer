def write_table_mpgp_analytics(data,filename,sep,endl):
    sep = sep+" "
    keys = ("Hessian","CG","expansion","proportioning","cost","fallbacks")
    row = data["analytics"][-1]
    #with open(filename,'w') as f:
    fname = filename.split('/')[-1][:-4]
    run = fname.split('_')
    with open("a.tex",'a') as f:
        f.write("{} {} {} {}".format(run[0],run[2],run[1],sep))
        for k in keys:
            if k == "fallbacks":
                sep = endl
            f.write("{} {}".format(row[k],sep))

def write_table_mpgp(data,filename,sep,endl):
    #fstring = "{{:.4e}}{}".format(sep)
    fstring = "\\num{{{{{{:.4e}}}}}}{}".format(sep)
    fstring = fstring*4
    fstring = "{{}}{}".format(sep) +fstring
    fstring = fstring[:-3] + " " +endl
    n = len(data["inner"])
    fname = filename.split('/')[-1][:-4]
    run = fname.split('_')
    header  = "\\begin{table}[h]\n\centering\n"
    header += "\\begin{tabular}{c | c c | c c}\n"
    header += "Step & Cost function & $g^P$ & $g^f$ & $g^c$ \\ \hline\n"
    footer  = "\end{tabular}\n"
    footer += "\caption{%s %s %s}\n" % (run[0],run[1],run[2])
    footer += "\label{tab:%s}\n" % (fname)
    footer += "\end{table}\n"
    with open(filename,'a') as f:
        f.write(header)
        for k in range(n):
            row = data["inner"][k]
            f.write(fstring.format(row["step"],row["cost"],row["gp"],row["gf"],row["gc"]))
        f.write(footer)

