from reader import reader
from viewer import plot
from viewer import text_table

def main(filename):
    #data = reader.reader_txt_smalxe(filename)
    #plot.plot_smalxe(data,"test")

    data = reader.reader_txt_mpgp(filename)
    filename = filename[:-4]
    plot.plot_mpgp(data,filename)
    text_table.write_table_mpgp_analytics(data,filename+".tex"," & ","// /hline \n")
    text_table.write_table_mpgp(data,filename+".tex"," & ","// /hline \n")

if __name__ == "__main__":
    import sys
    main(sys.argv[1])

